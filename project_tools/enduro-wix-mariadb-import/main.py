import sys
import re

import pandas as pd
from pandas import DataFrame, Series
import numpy as np
import mariadb

from Configurations.Configuration import Configuration


class Main:
    CONFIGURATION_FILE = 'config.ini'

    def __init__(self):
        self.configuration = None
        self.connection = None
        self.cursor = None

        # Load configuration
        self.configuration = Configuration.create(self.CONFIGURATION_FILE)

        # Connect to database
        self.connection = mariadb.connect(
            user=self.configuration.database.user,
            password=self.configuration.database.password,
            host=self.configuration.database.host,
            port=self.configuration.database.port,
            database=self.configuration.database.database
        )

        self.cursor = self.connection.cursor()

    def __del__(self):
        if self.connection:
            self.connection.close()

    def run(self):
        # Load csv into pandas
        dataframe = pd.read_csv(self.configuration.csv_file, sep=';')

        # Create event
        id_event = self.create_event(
            event_name=self.configuration.event_name,
            event_start_date='2024-05-04',
            event_end_date='2024-05-04'
        )
        # Create ticket_types
        ticket_id_dict = self.create_ticket_types(id_event, dataframe)

        # Create tickets and their associated participants and vehicles
        self.create_tickets(dataframe, ticket_id_dict)

    def create_event(self, event_name: str, event_start_date: str, event_end_date: str) -> int:
        try:
            self.cursor.execute(f"""
                INSERT INTO events (
                    name,
                    start_date,
                    end_date
                ) VALUES (?, ?, ?)
                """, (
                    event_name,
                    event_start_date,
                    event_end_date
                )
            )
            self.connection.commit()
            return self.cursor.lastrowid
        except Exception as e:
            self.connection.rollback()
            raise e

    def create_ticket_types(self, id_event: int, dataframe: DataFrame) -> dict:
        # Get list of each unique ticket names and prices
        unique_prices_by_name = dataframe.groupby(["Ticket type"]).first()

        ticket_id_dict = {}

        for index, row in unique_prices_by_name.iterrows():
            name = index
            ticket_price = row['Ticket price'] if row['Ticket price'] is not np.nan else None
            # remove "€ " from ticket price and convert to float
            ticket_price = float(ticket_price.replace('€ ', ''))

            try:
                self.cursor.execute(f"""
                    INSERT INTO ticket_types (
                        event_id,
                        name,
                        price
                    ) VALUES (?, ?, ?)
                    """, (
                        id_event,
                        name,
                        ticket_price
                    )
                )
                self.connection.commit()
                # Add ticket_id to dictionary
                ticket_id = self.cursor.lastrowid
                ticket_id_dict[name] = ticket_id
            except Exception as e:
                self.connection.rollback()
                raise e

        return ticket_id_dict

    def create_tickets(self, dataframe: DataFrame, ticket_id_dict: dict):
        # For each row of the dataframe, create the participant, the vehicle, the ticket, and the participant_ticket.
        for index, row in dataframe.iterrows():
            # Skip the row if the ticket type is "Repas supplémentaire"
            if row["Ticket type"] == "Repas supplémentaire":
                continue

            participant_id = self.create_participant(row)
            vehicle_id = self.create_vehicle(row)
            ticket_id = self.create_ticket(row, ticket_id_dict, vehicle_id)
            role_name = "Conducteur"

            # Link participant and ticket
            try:
                self.cursor.execute(f"""
                    INSERT INTO participants_tickets (
                        participants_id,
                        tickets_id,
                        role_name
                    ) VALUES (?, ?, ?)
                    """, (
                        participant_id,
                        ticket_id,
                        role_name
                    )
                )
                self.connection.commit()
            except Exception as e:
                self.connection.rollback()
                raise e

    def create_participant(self, row: Series) -> int:
        full_name = row['Guest name'] if row['Guest name'] is not np.nan else ''
        email = row['Email'] if row['Email'] is not np.nan else ''
        phone = row['Téléphone'] if row['Téléphone'] is not np.nan else ''
        street_name = row['Adresse'] if row['Adresse'] is not np.nan else ''

        # Remove multiple spaces in full name
        full_name = re.sub(' +', ' ', full_name)

        has_extra_space_in_full_name = full_name.count(' ') > 1
        first_name, last_name = full_name.split(' ', 1)

        try:
            self.cursor.execute(f"""
                INSERT INTO participants (
                    first_name,
                    last_name,
                    mail,
                    phone_number,
                    street_name
                ) VALUES (?, ?, ?, ?, ?)
                """, (
                    first_name,
                    last_name,
                    email,
                    phone,
                    street_name
                )
            )
            self.connection.commit()

            participant_id = self.cursor.lastrowid

            if has_extra_space_in_full_name:
                print(
                    f"Participant ID[{participant_id}] has an extra space in their name ({full_name}), please manually update the participant's name.")
            return participant_id

        except Exception as e:
            self.connection.rollback()
            raise e

    def create_vehicle(self, row: Series) -> int:
        plate_number = row["Plaque d'immatriculation"] if row["Plaque d'immatriculation"] is not np.nan else ''
        # Retrieve the vehicle type from the Ticket type column
        vehicle_type = row['Ticket type'] if row['Ticket type'] is not np.nan else ''
        # Remove "Randonnée" and "Balade" from the ticket type
        vehicle_type = vehicle_type.replace('Randonnée', '')
        vehicle_type = vehicle_type.replace('Balade', '')
        # Remove "+ passager" from the ticket type
        vehicle_type = vehicle_type.replace('+ passager', '')
        # Remove numbers from the vehicle type (eg: "2024" from "2024 Moto")
        vehicle_type = ''.join([i for i in vehicle_type if not i.isdigit()])
        vehicle_type = vehicle_type.strip()

        try:
            self.cursor.execute(f"""
                INSERT INTO vehicles (
                    plate_number,
                    type
                ) VALUES (?, ?)
                """, (
                    plate_number,
                    vehicle_type
                )
            )
            self.connection.commit()
            return self.cursor.lastrowid
        except Exception as e:
            self.connection.rollback()
            raise e

    def create_ticket(self, row: Series, ticket_type_id_dict: dict, vehicle_id: int) -> int:
        ticket_number = row['Ticket number']
        payment_status = row['Payment status']

        order_number = row['Order number']
        order_date = row['Order date']

        # convert from 18/03/2024 12:07 to 2024-03-18 12:07 format (SQL datetime format)
        order_date = pd.to_datetime(row['Order date'], format="%d/%m/%Y %H:%M")
        order_date = order_date.strftime("%Y-%m-%d %H:%M")

        meal_tickets = 1

        # Get ticket_type id from dictionary
        ticket_type = row['Ticket type']
        ticket_type_id = ticket_type_id_dict[ticket_type]

        try:
            self.cursor.execute(f"""
                INSERT INTO tickets (
                    ticket_types_id,
                    vehicles_id,
                    ticket_number,
                    payment_status,
                    order_number,
                    order_date,
                    meal_tickets
                ) VALUES (?, ?, ?, ?, ?, ?, ?)
                """, (
                    ticket_type_id,
                    vehicle_id,
                    ticket_number,
                    payment_status,
                    order_number,
                    order_date,
                    meal_tickets
                )
            )
            self.connection.commit()
            return self.cursor.lastrowid
        except Exception as e:
            self.connection.rollback()
            raise e


if __name__ == '__main__':
    try:
        main = Main()
        main.run()
    except Exception as e:
        print(f'An error occurred: {e}')
        sys.exit(1)

    sys.exit(0)
