import configparser
import copy
from typing import NamedTuple


class Database(NamedTuple):
    """
    Database
    NamedTuple record for database configuration
    Attributes:
        host (str): Database host
        port (int): Database port
        user (str): Database user
        password (str): Database password
        database (str): Database name
    """
    host: str
    port: int
    user: str
    password: str
    database: str


class ConfigurationDTO(NamedTuple):
    """
    ConfigurationDTO
    NamedTuple record for configuration
    Attributes:
        database (Database): Database configuration
        csv_file (str): CSV file path
        event_name (str): event name
    """
    database: Database
    csv_file: str
    event_name: str



class Configuration:
    """
    Configuration
    Configuration class to read configuration file
    Attributes:
        __config_file (str): Configuration file path
        __config (ConfigParser): ConfigParser object
        database (Database): Database configuration
        csv_file (str): CSV file path
    """

    def __init__(self, config_file: str):
        """
        Constructor
        """
        self.__config_file = config_file

        self.__config = configparser.ConfigParser()
        self.__config.read(config_file, encoding="utf-8")

        self.database = Database(
            host=self.__config['database']['host'],
            port=self.__config.getint('database', 'port'),
            user=self.__config['database']['user'],
            password=self.__config['database']['password'],
            database=self.__config['database']['database']
        )

        self.csv_file = self.__config['csv']['file']
        self.event_name = self.__config['general']['event']

    def to_record(self) -> ConfigurationDTO:
        """
        to_record
        Convert the configuration to a ConfigurationDTO
        Returns:
            ConfigurationDTO: ConfigurationDTO record
        """
        return ConfigurationDTO(
            database=copy.deepcopy(self.database),
            csv_file=self.csv_file,
            event_name=self.event_name
        )

    @staticmethod
    def create(config_file: str) -> ConfigurationDTO:
        """
        create
        Static factory method to create a configuration from a file
        Args:
            config_file (str): Configuration file path
        Returns:
            ConfigurationDTO: ConfigurationDTO record
        """
        configuration = Configuration(config_file)
        return configuration.to_record()
