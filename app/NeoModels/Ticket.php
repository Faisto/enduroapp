<?php

namespace App\NeoModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\DTOs\Tickets\TicketDTO;
use App\DTOs\Tickets\NestedTicketDTO;
use App\NeoModels\Participant;


class Ticket extends Model
{
    protected $fillable = [
        'ticket_number',
        'vehicles_id',
        'payment_status',
        'purchase_number',
        'ticket_types_id',
        'purchase_date',
        'meal_tickets',
        'comment'
    ];

    public function dossards(): HasMany
    {
        return $this->hasMany(Dossard::class, 'id_ticket');
    }

    public function ticketType(): BelongsTo
    {
        return $this->BelongsTo(TicketType::class, 'ticket_types_id');
    }

    // TODO: Fix to use RoleParticipantTicket instead.
    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(
            Participant::class,
            'participants_tickets',
            'tickets_id',
            'participants_id'
        );
    }

    // TODO : Add the missing relationships.

    public function toDTO(): TicketDTO
    {
        // Place all participants in a collection
        $participants = $this->participants->map(function ($participant) {
            return $participant->toNestedDTO();
        });

        return new TicketDTO(
            $this->id,
            $participants,
            $this->ticket_number,
            $this->comment,
        );
    }

    /*public function toNestedDTO(): NestedTicketDTO
    {
        // Place all participants in a collection
        $participants = $this->participants->map(function ($participant) {
            return $participant->toNestedDTO();
        });

        return new NestedTicketDTO(
            $this->id,
            $participants
        );
    }*/

}
