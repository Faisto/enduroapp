<?php

namespace App\NeoModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\DTOs\Participants\NestedParticipantDTO;

class Participant extends Model {

    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'mail',
        'street_name',
        'house_number',
        'zip_code',
        'city',
        'state',
        'country',
        'phone_number',
        'date_of_birth',
        'information'
    ];

    // TODO: Fix to use RoleParticipantTicket instead.
    public function tickets(): BelongsToMany
    {
        return $this->belongsToMany(
            Ticket::class,
            'participants_tickets',
            'participants_id',
            'tickets_id'
        );
    }

    // TODO : Add the missing relationships


    public function toNestedDTO(): NestedParticipantDTO
    {
        // Place all tickets id in a collection
        $tickets = $this->tickets->map(function ($ticket) {
            return $ticket->id;
        });

        return new NestedParticipantDTO(
            $this->id,
            $tickets,
            $this->first_name,
            $this->last_name,
            $this->mail,
            $this->street_name,
            $this->house_number,
            $this->zip_code,
            $this->city,
            $this->state,
            $this->country,
            $this->phone_number,
            $this->date_of_birth,
            $this->information,
        );
    }
}
