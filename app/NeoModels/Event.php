<?php

namespace App\NeoModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'inscription_date',
        'vip_date',
        'description',
        'information'
    ];

    public function ticketTypes() : HasMany
    {
        return $this->hasMany(TicketType::class);
    }

    // TODO : Add the missing relationships
}
