<?php

namespace App\NeoModels;

use App\DTOs\Dossards\DossardDTO;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Dossard extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_ticket',
        'number',
        'color_number',
        'background_color',
        'attribution_date',
        'return_date'
    ];

    public function ticket() : BelongsTo
    {
        return $this->belongsTo(Ticket::class, 'id_ticket');
    }

    public function toDTO(): DossardDTO
    {
        $ticket = $this->ticket->toDTO();

        return new DossardDTO(
            $this->id,
            $this->number,
            DossardColorNumber::from($this->color_number),
            DossardBackgroundColor::from($this->background_color),
            Carbon::parse($this->attribution_date),
            Carbon::parse($this->return_date),
            $this->created_at,
            $this->updated_at,
            $ticket
        );
    }
}
