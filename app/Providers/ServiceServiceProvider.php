<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\Services\IDossardService;
use App\Services\DossardService;

/**
 * Class ServiceServiceProvider
 * @package App\Providers
 *
 * Service provider for the services, binds the interfaces to the implementations
 */
class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Bind the DossardService to the IDossardService
        // This way, when we ask for an IDossardService, we get a DossardService, in our controllers for example.
        $this->app->bind(
            IDossardService::class,
            DossardService::class
        );
    }
}
