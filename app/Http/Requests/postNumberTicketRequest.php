<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class postNumberTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'ticketnumber' => ['required', 'min: 13', 'max:13', 'regex:/^[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{3}$/' ]
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
           'ticketnumber' =>$this->input('ticketnumber')
        ]);

    }
}
