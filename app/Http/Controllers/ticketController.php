<?php

namespace App\Http\Controllers;

use App\Models\tickets;
use DateTime;
use Illuminate\Http\Request;

class ticketController extends Controller
{
    //Get ticket from id
    public function getTicketById($id) : mixed {
        //Get ticket from db
        $ticket = tickets::findById($id);
        return $ticket;
    }

    public function findByParticipantName($participant_last_name, $participant_first_name) : tickets
    {
        //Get ticket from db
        return tickets::findByParticipantName($participant_last_name, $participant_first_name);
    }

    public function validateTicket($id, $status, $comment) : bool {
        try {
            //Get ticket from db
            $ticket = tickets::findById($id);


            // If the payment status changed, change the purchase date
            if ($status->value != $ticket->payment_status) {
                $ticket->order_date = new DateTime();

                //Set payment status
                $ticket->payment_status = $status->value;
            }

            //Set comment
            $ticket->comment = $comment;

            //Save ticket
            $ticket->save();

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }
}
