<?php

namespace App\Http\Controllers;

use App\Models\tickets;
use Illuminate\Http\Request;
use App\Models\dossards;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class dossardsController extends Controller
{
    public function DossardAttributionView($idevent,$idTicket): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {

        $testing = ['idevent' => $idevent, 'idTicket' =>$idTicket];

        return view('DossardAttribution', [
            "test" => $testing
        ]);
    }

    public function linkDossardToTicket($idevent, $idTicket){

        $dossard = dossards::findByTicketId($idTicket);
        $ticker = tickets::findById($idTicket);
        if($dossard != null){
            return redirect('/event/'.$idevent)->withErrors('Ce ticket a déjà été assigné à un dossard');
        } else {
            $dossard = dossards::factory()->create([
                'id_ticket' => $ticker->id,
                'number' =>$_POST['number'],
                'background_color' => $_POST['bg_color'],
                'color_number' => $_POST['color_number'],
                'attribution_date' => date('Y-m-d H:i:s'),
                'return_date' => null
            ]);
        }
        return redirect('/event/'.$idevent)->with('success', 'Dossard attribué');
    }

    public function search(Request|\http\Env\Request $request)
    {
        $dossard = dossards::where('number', $request->dossard)->first();
        if ($dossard) {
            return view('resultat', ['dossard' => $dossard]);
        } else {
            return redirect('/sortie')->with('error', 'Dossard non trouvé');
        }
    }

    public function refund(Request $request)
    {
        $dossard = dossards::find($request->dossard_id);
        if ($dossard) {
            $dossard->return_date = date('Y-m-d');
            $dossard->save();

            if ($dossard->return_date) {
                return redirect('/sortie')->with('success', 'Dossard remboursé et la date de remboursement a été enregistrée : ' . $dossard->return_date);
            } else {
                return redirect('/sortie')->with('error', 'Dossard remboursé mais la date de remboursement n\'a pas été enregistrée');
            }
        } else {
            return redirect('/sortie')->with('error', 'Dossard non trouvé');
        }
    }

    public function ReturnDossard(int $idevent,int $NBDossard, String $ColorNumber,String $BgColor)
    {
        $Updated = DB::table('dossards')
            ->where('number',$NBDossard)('color_number',$ColorNumber) ('background_color',$BgColor)
            ->update(['return_date' => date("Y/m/d H:i:s")]);

        return redirect('/events/'.$idevent);

    }
}
