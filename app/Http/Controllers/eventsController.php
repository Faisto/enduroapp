<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class eventsController extends Controller
{
    public function indexEvents() {
        // TODO : Fix this method to instead permit the user to select an event.

        // Temporarily return the first event in the database
        $event = \App\Models\events::first();

        // Redirect to the event page
        return redirect('/events/' . $event->id);
    }
    public function Events($idevent){

        $testing = ['idevent' => $idevent];

        return view('Events', [
            "test" => $testing
        ]);
    }
}
