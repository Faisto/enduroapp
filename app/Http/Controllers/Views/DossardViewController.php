<?php

namespace App\Http\Controllers\Views;

use App\DTOs\Dossards\PatchDossardDTO;
use App\NeoModels\Dossard;
use Exception;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\NeoModels\Event;
use App\Contracts\Services\IDossardService;
use App\DTOs\Dossards\SearchDossardDTO;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;


/**
 * Class DossardViewController
 * @package App\Http\Controllers\Views
 *
 * Controller for the Dossard views
 */
class DossardViewController extends ViewController {
    private IDossardService $dossardService;

    public function __construct(
        IDossardService $dossardService
    ) {
        $this->dossardService = $dossardService;
    }

    public function index(Event $event, Request $request) {
        // Build the SearchDossardDTO from the request
        $searchDossardDTO = new SearchDossardDTO();

        $searchDossardDTO->eventId = $event->id;

        // Get query parameters
        $hasQuery = count($request->query()) > 0;
        $numberQuery = $request->query('number');
        $colorNumberQuery = $request->query('color-number');
        $backgroundColorQuery = $request->query('background-color');

        $searchDossardDTO->number = $numberQuery;

        // Check if the color-number is set and the value isn't empty.
        if ($colorNumberQuery && $colorNumberQuery !== '') {
            try {
                $searchDossardDTO->colorNumber = DossardColorNumber::from($colorNumberQuery);
            } catch (Exception $e) {
                // Return a 422 error if the numberColor is invalid
                $validValues = implode(', ', DossardColorNumber::cases());
                return response()->view('errors.422', [
                    'message' => 'Invalid colorNumber value, must be one of: ' . $validValues,
                ], 422);
            }
        }

        // Check if the background-color is set and the value isn't empty.
        if ($backgroundColorQuery && $backgroundColorQuery !== '') {
            try {
                $searchDossardDTO->backgroundColor = DossardBackgroundColor::from($backgroundColorQuery);
            } catch (Exception $e) {
                // Return a 422 error if the numberColor is invalid
                $validValues = implode(', ', DossardBackgroundColor::cases());
                return response()->view('errors.422', [
                    'message' => 'Invalid backgroundColor value, must be one of: ' . $validValues,
                ], 422);
            }
        }

        // TODO : Provide the ability to specify these fields in the form.
        $searchDossardDTO->isAttributed = true; // $request->input('isAttributed');
        $searchDossardDTO->isReturned = false;


        if ($hasQuery) {
            // Get the dossards from the service
            $dossards = $this->dossardService->searchDossards($searchDossardDTO);
        }
        else {
            // If no query parameters are set, return empty collection.
            $dossards = collect();
        }

        $request->flash();

        return view(
            'dossards.index',
            [
                'event' => $event,
                'dossards' => $dossards,
            ]
        );
    }

    /**
     * Return a dossard
     *
     * @param Event $event
     * @param Dossard $dossard
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function return (Event $event, Dossard $dossard, Request $request) {
        $patchDossardDTO = new PatchDossardDTO();
        $patchDossardDTO->returnDate = now();

        $this->dossardService->patchDossard($dossard->id, $patchDossardDTO);

        // Redirect to the same page who called the return method
        return redirect()->back()->withInput();
    }

}
