<?php

namespace App\Http\Controllers;

use App\Enums\payementStatus;
use App\Http\Requests\postNumberTicketRequest;
use Illuminate\Http\Request;

class GestionEventTicketsController extends Controller
{

    public function showSelectEventViews ( ): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $firstEvent = \App\Models\events::first();

        return view('selectEvent', [
            "firstEvent" => $firstEvent
        ]);
    }
    public function selectEvent(): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        \request()->session()->put('idEvent', $_POST['idEvent']);
        //return redirect('/event/'.$_POST['idEvent']);
        return redirect()->route('homeRoute');
    }
    public function selectTicketControl (Request $request): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        //$post = Post::create($ticketRequest->validated());

        $idEvent = $request->session()->get('idEvent');

        \request()->session()->put('ticketNumber', $_POST['ticketNumber']);
        return redirect('events/'.$idEvent.'/tickets/'.$_POST['ticketNumber']);
    }

}
