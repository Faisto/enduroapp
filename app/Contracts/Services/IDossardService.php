<?php

namespace App\Contracts\Services;

use Illuminate\Support\Collection;

use App\DTOs\Dossards\CreateDossardDTO;
use App\DTOs\Dossards\DossardDTO;
use App\DTOs\Dossards\PatchDossardDTO;
use App\DTOs\Dossards\SearchDossardDTO;
use App\DTOs\Dossards\UpdateDossardDTO;

interface IDossardService
{
    /*
     * Get all dossards
     * @return Collection<DossardDTO>
     */
    public function getAllDossards(): Collection;

    /*
     * Get all dossards that match the search criteria
     * @param SearchDossardDTO $searchDossardDTO
     * @return Collection<DossardDTO>
     */
    public function searchDossards(SearchDossardDTO $searchDossardDTO): Collection;

    /*
     * Return a dossard by its id
     * @param int $id
     * @return ?DossardDTO
     */
    public function getDossardById(int $id): ?DossardDTO;

    /*
     * Create a new dossard
     * @param DossardDTO $dossardDTO
     * @return DossardDTO
     */
    public function createDossard(CreateDossardDTO $dossardDTO): DossardDTO;

    /*
     * Update all the values of a a dossard
     * @param int $number
     * @param UpdateDossardDTO $dossardDTO
     * @return ?DossardDTO
     */
    public function updateDossard(int $id, UpdateDossardDTO $dossardDTO): ?DossardDTO;

    /*
     * Patch specific values of a dossard
     * @param int $id
     * @param PatchDossardDTO $dossardDTO
     * @return ?DossardDTO
     */
    public function patchDossard(int $id, PatchDossardDTO $dossardDTO): ?DossardDTO;

    /*
     * Delete a dossard by its id
     * @param int $id
     * @return void
     */
    public function deleteDossard(int $id): void;
}
