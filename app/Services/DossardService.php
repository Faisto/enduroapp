<?php

namespace App\Services;

use Illuminate\Support\Collection;
use App\Contracts\Services\IDossardService;
use App\DTOs\Dossards\DossardDTO;
use App\DTOs\Dossards\CreateDossardDTO;
use App\DTOs\Dossards\UpdateDossardDTO;
use App\DTOs\Dossards\PatchDossardDTO;
use App\DTOs\Dossards\SearchDossardDTO;
use App\NeoModels\Dossard;

class DossardService implements IDossardService
{
    /*
     * Get all dossards
     * @return Collection<DossardDTO>
     */
    public function getAllDossards(): Collection
    {
        // TODO
        return collect([]);
    }

    /*
     * Get all dossards that match the search criteria
     * @param SearchDossardDTO $searchDossardDTO
     * @return Collection<DossardDTO>
     */
    public function searchDossards(SearchDossardDTO $searchDossardDTO): Collection
    {
        // Search the dossards in the database using the search criteria.
        $query = Dossard::query();
        if ($searchDossardDTO->number)
            $query = $query->where('number', $searchDossardDTO->number);

        if ($searchDossardDTO->colorNumber)
            $query = $query->where('color_number', $searchDossardDTO->colorNumber->value);

        if ($searchDossardDTO->backgroundColor)
            $query = $query->where('background_color', $searchDossardDTO->backgroundColor->value);

        if ($searchDossardDTO->isAttributed)
            $query = $query->whereNotNull('attribution_date');
        else
            $query = $query->whereNull('attribution_date');

        if ($searchDossardDTO->isReturned)
            $query = $query->whereNotNull('return_date');
        else
            $query = $query->whereNull('return_date');

        if ($searchDossardDTO->eventId) {
            // Search the dossards by the event related to the ticket and its ticket_type
            $query = $query->whereHas('ticket.ticketType.event', function ($query) use ($searchDossardDTO) {
                $query->where('id', $searchDossardDTO->eventId);
            });
        }

        $dossards = $query->get();

        // Map the dossards to DTOs
        return $dossards->map(
            function ($dossard) {
                return $dossard->toDTO();
            }
        );
    }

    /*
     * Return a dossard by its id
     * @param int $id
     * @return ?DossardDTO
     */
    public function getDossardById(int $id): ?DossardDTO
    {
        // TODO
        return null;
    }

    /*
     * Create a new dossard
     * @param DossardDTO $dossardDTO
     * @return DossardDTO
     */
    public function createDossard(CreateDossardDTO $dossardDTO): DossardDTO
    {
        // TODO
        return new DossardDTO();
    }

    /*
     * Update all the values of a a dossard
     * @param int $number
     * @param UpdateDossardDTO $dossardDTO
     * @return ?DossardDTO
     */
    public function updateDossard(int $id, UpdateDossardDTO $dossardDTO): ?DossardDTO
    {
        // TODO
        return null;
    }

    /*
     * Patch specific values of a dossard
     * @param int $id
     * @param PatchDossardDTO $dossardDTO
     * @return ?DossardDTO
     */
    public function patchDossard(int $id, PatchDossardDTO $dossardDTO): ?DossardDTO
    {
        // Get the dossard by its id
        $dossard = Dossard::findOrfail($id);

        // Update the dossard with the specified values
        if ($dossardDTO->returnDate)
            $dossard->return_date = $dossardDTO->returnDate;

        // TODO:  all the other fields

        $dossard->save();

        return $dossard->toDTO();
    }

    /*
     * Delete a dossard by its id
     * @param int $id
     * @return void
     */
    public function deleteDossard(int $id): void
    {
        // TODO
    }

}
