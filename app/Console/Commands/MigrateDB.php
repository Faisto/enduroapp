<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:migrate-d-b';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'migrate in the right order the tables of the database';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //migrate table in order
        $migrations = [
            '2024_04_30_121817_create_vehicles_table.php',
            '2024_04_30_121255_create_participants_table.php',
            '2024_04_30_121053_create_events_table.php',
            '2024_04_30_121434_create_ticket_types_table.php',
            '2024_04_30_121531_create_tickets_table.php',
            '2024_04_30_121023_create_dossards_table.php',
            '2024_04_30_121216_create_groups_table.php',
            '2024_04_30_121854_create_workers_table.php',
            '2024_04_30_124204_create_group_events_table.php',
            '2024_04_30_124252_create_role_participants_tickets_table.php',
            '2024_04_30_124252_create_role_participants_tickets_table.php',
            '2024_04_30_124504_create_role_worker_event_table.php',
            '2024_05_01_170740_create_sessions_table.php',
        ];

        foreach($migrations as $migration)
        {
            $basePath = 'database/migrations/';
            $migrationName = trim($migration);
            $path = $basePath.$migrationName;
            $this->call('migrate', [
                '--path' => $path ,
            ]);
        }

    }
}
