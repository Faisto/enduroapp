<?php

namespace App\Enums\Dossards;

enum DossardBackgroundColor: string
{
    case RED = 'red';
    case BLUE = 'blue';
    case GREEN = 'green';
}
