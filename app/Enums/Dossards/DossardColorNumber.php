<?php

namespace App\Enums\Dossards;

enum DossardColorNumber: string
{
    case WHITE = 'white';
    case BLACK = 'black';
}
