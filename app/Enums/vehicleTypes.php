<?php

namespace App\Enums;

enum vehicleTypes: string
{
    case moto = 'moto';
    case quad = 'quad';
    case SSV = 'SSV';
}
