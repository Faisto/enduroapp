<?php

namespace App\Enums;

enum dossardsColor: string
{
    case red = 'red';
    case blue = 'blue';
    case green = 'green';
}
