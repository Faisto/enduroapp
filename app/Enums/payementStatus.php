<?php

namespace App\Enums;

enum payementStatus: string
{
    case paid = 'paid';
    case unpaid = 'unpaid';
}
