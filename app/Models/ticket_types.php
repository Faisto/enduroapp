<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ticket_types extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'price',
        'event_id'
    ];

    public function events() : BelongsTo
    {
        return $this->belongsTo(events::class);
    }

    public function ticket() : HasMany
    {
        return $this->hasMany(tickets::class);
    }
}
