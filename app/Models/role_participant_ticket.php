<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class role_participant_ticket extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'role_id',
        'participant_id',
        'ticket_id'
    ];
}
