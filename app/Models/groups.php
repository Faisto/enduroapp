<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;

class groups extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    public function events() : BelongsToMany
    {
        return $this->belongsToMany(events::class);
    }

    public function participants() : BelongsToMany
    {
        return $this->belongsToMany(participants::class);
    }
}
