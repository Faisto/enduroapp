<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class dossards extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'id_ticket',
        'number',
        'color_number',
        'background_color',
        'attribution_date',
        'return_date'
    ];

    public static function findByTicketId($idTicket)
    {
        //Get ticket from db
        $ticketId = tickets::all()->where('ticket_number', $idTicket)->first();
        return dossards::all()->firstWhere('id_ticket', $ticketId['id']);
    }


    public function tickets() : HasOne
    {
        return $this->hasOne(tickets::class);
    }
}
