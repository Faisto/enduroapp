<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class participants extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'mail',
        'street_name',
        'house_number',
        'zip_code',
        'city',
        'state',
        'country',
        'phone_number',
        'date_of_birth',
        'information'
    ];

    public function groups() : BelongsToMany
    {
        return $this->belongsToMany(groups::class);
    }

    public function ticket() : BelongsToMany
    {
        return $this->belongsToMany(tickets::class);
    }
}
