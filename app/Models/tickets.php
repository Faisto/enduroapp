<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use PhpParser\Node\Scalar\String_;

class tickets extends Model
{
    use HasFactory;


    protected $fillable = [
        'id',
        'ticket_number',
        'vehicles_id',
        'payment_status',
        'order_number',
        'ticket_types_id',
        'order_date',
        'meal_tickets',
        'comment'
    ];

    public function vehicles() : BelongsTo
    {
        return $this->belongsTo(vehicles::class);
    }

    public function participants() : BelongsToMany
    {
        return $this->belongsToMany(participants::class);
    }

    public function ticket_types() : BelongsTo
    {
        return $this->belongsTo(ticket_types::class);
    }

    public function dossards() : HasOne
    {
        return $this->hasOne(dossards::class);
    }

    public static function find($id)
    {
        //Get ticket from db
        return tickets::all()->firstWhere('ticket_number', $id);
    }

    public static function findByParticipantName($participant_first_name, $participant_last_name) : tickets
    {
        //Get id of participant from id
        $participant_id = participants::all()->select('id')->where('first_name', $participant_first_name)->where('last_name', $participant_last_name);
        echo $participant_id;
        //Get ticket from db
        $ticket_id = role_participant_ticket::all()->select('id')->where('participant_id', $participant_id);
        echo $ticket_id;
        return tickets::all()->firstWhere('id', $ticket_id);
    }

    public static function findById($id) : mixed
    {
        //Get ticket from db
        return tickets::all()->firstWhere('ticket_number', $id);
    }

    public function setPaymentStatus($status): bool
    {
        return tickets::update(['payment_status' => $status]);
    }

    public function setComment($comment): bool{
        return tickets::update(['comment' => $comment]);
    }
}
