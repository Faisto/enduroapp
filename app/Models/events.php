<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class events extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'start_date',
        'end_date',
        'inscription_date',
        'vip_date',
        'description',
        'information'
    ];

    public function tickets_types() : HasMany
    {
        return $this->hasMany(ticket_types::class);
    }

    public function worker() : BelongsToMany
    {
        return $this->belongsToMany(workers::class);
    }

    public function groups() : BelongsToMany
    {
        return $this->belongsToMany(groups::class);
    }
}
