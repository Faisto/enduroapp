<?php

namespace App\DTOs\Tickets;

use Illuminate\Support\Collection;

/**
 * Class TicketDTO
 * @package App\DTOs\Tickets
 *
 * Data Transfer Object for Ticket
 */
class TicketDTO
{
    public int $id;
    public Collection $participants;
    public string $ticketNumber;
    public string $comment;

    public function __construct(
        int $id,
        Collection $participants,
        string $ticketNumber,
        string $comment
    ) {
        $this->id = $id;
        $this->participants = $participants;
        $this->ticketNumber = $ticketNumber;
        $this->comment = $comment;
    }
}
