<?php

namespace App\DTOs\Tickets;

use Illuminate\Support\Collection;

/**
 * Class NestedTicketDTO
 * @package App\DTOs\Tickets
 *
 * Data Transfer Object for Nested Ticket, to be used in other DTOs
 */
class NestedTicketDTO {
    public int $id;
    public Collection $participants;

    public function __construct(
        int $id,
        Collection $participants,
    ) {
        $this->id = $id;
        $this->participants = $participants;
    }
}
