<?php

namespace App\DTOs\Dossards;

use DateTime;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/**
 * Class PatchDossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for patching specific values of a Dossard
 *
 */
class PatchDossardDTO {
    public ?int $ticketId = null;
    public ?int $number = null;
    public ?DossardColorNumber $colorNumber = null;
    public ?DossardBackgroundColor $backgroundColor = null;
    public ?DateTime $attributionDate = null;
    public ?DateTime $returnDate = null;
}
