<?php

namespace App\DTOs\Dossards;

use DateTime;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/**
 * Class UpdateDossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for updating every value of a Dossard
 *
 */
class UpdateDossardDTO {
    public int $ticketId;
    public int $number;
    public DossardColorNumber $colorNumber;
    public DossardBackgroundColor $backgroundColor;
    public DateTime $attributionDate;
    public DateTime $returnDate;

    public function __construct(
        int $ticketId,
        int $number,
        DossardColorNumber $colorNumber,
        DossardBackgroundColor $backgroundColor,
        DateTime $attributionDate,
        DateTime $returnDate
    ) {
        $this->ticketId = $ticketId;
        $this->number = $number;
        $this->colorNumber = $colorNumber;
        $this->backgroundColor = $backgroundColor;
        $this->attributionDate = $attributionDate;
        $this->returnDate = $returnDate;
    }
}
