<?php

namespace App\DTOs\Dossards;

use App\DTOs\Tickets\TicketDTO;
use DateTime;
use App\DTOs\Tickets\NestedTicketDTO;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/**
 * Class DossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for Dossard
 */
class DossardDTO {
    public int $id;

    public int $number;
    public DossardColorNumber $colorNumber;
    public DossardBackgroundColor $backgroundColor;

    public DateTime $attributionDate;
    public DateTime $returnDate;
    public DateTime $createdAt;
    public DateTime $updatedAt;
    public TicketDTO $ticket;
    //public NestedTicketDTO $ticket;

    public function __construct(
        int $id,
        int $number,
        DossardColorNumber $colorNumber,
        DossardBackgroundColor $backgroundColor,
        DateTime $attributionDate,
        DateTime $returnDate,
        DateTime $createdAt,
        DateTime $updatedAt,
        TicketDTO $ticket
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->colorNumber = $colorNumber;
        $this->backgroundColor = $backgroundColor;
        $this->attributionDate = $attributionDate;
        $this->returnDate = $returnDate;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->ticket = $ticket;
    }
}
