<?php

namespace App\DTOs\Dossards;

use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/**
 * Class SearchDossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for searching Dossards based on specific criteria
 *
 */
class SearchDossardDTO {
    public ?int $eventId = null;

    public ?bool $isAttributed = null;
    public ?bool $isReturned = null;
    public ?int $number = null;
    public ?DossardColorNumber $colorNumber = null;
    public ?DossardBackgroundColor $backgroundColor = null;

    /*
    public ?int $ticketId = null;
    public ?DateTime $attributionDate = null;
    public ?DateTime $returnDate = null;
    public ?DateTime $createdAt = null;
    public ?DateTime $updatedAt = null;
    */
}
