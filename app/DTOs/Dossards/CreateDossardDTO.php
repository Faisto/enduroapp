<?php

namespace App\DTOs\Dossards;

use DateTime;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/**
 * Class CreateDossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for creating a new Dossard
 *
 */
class CreateDossardDTO {
    public int $ticketId;
    public int $number;
    public DossardColorNumber $colorNumber;
    public DossardBackgroundColor $backgroundColor;
    public ?DateTime $attributionDate = null;
    public ?DateTime $returnDate = null;

    public function __construct(
        int $ticketId,
        int $number,
        DossardColorNumber $colorNumber,
        DossardBackgroundColor $backgroundColor,
        ?DateTime $attributionDate = null,
        ?DateTime $returnDate = null
    ) {
        $this->ticketId = $ticketId;
        $this->number = $number;
        $this->colorNumber = $colorNumber;
        $this->backgroundColor = $backgroundColor;
        $this->attributionDate = $attributionDate;
        $this->returnDate = $returnDate;
    }
}
