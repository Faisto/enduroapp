<?php

namespace App\DTOs\Dossards;

use DateTime;
use App\Enums\Dossards\DossardBackgroundColor;
use App\Enums\Dossards\DossardColorNumber;

/*
 * Class NestedDossardDTO
 * @package App\DTOs\Dossards
 *
 * Data Transfer Object for Nested Dossard, to be used in other DTOs
 *
*/
class NestedDossardDTO {
    public int $id;
    public int $ticketId;
    public int $number;
    public DossardColorNumber $colorNumber;
    public DossardBackgroundColor $backgroundColor;
    public DateTime $attributionDate;
    public DateTime $returnDate;
    public DateTime $createdAt;
    public DateTime $updatedAt;

    public function __construct(
        int $id,
        int $ticketId,
        int $number,
        DossardColorNumber $colorNumber,
        DossardBackgroundColor $backgroundColor,
        DateTime $attributionDate,
        DateTime $returnDate,
        DateTime $createdAt,
        DateTime $updatedAt,
    ) {
        $this->id = $id;
        $this->ticketId = $ticketId;
        $this->number = $number;
        $this->colorNumber = $colorNumber;
        $this->backgroundColor = $backgroundColor;
        $this->attributionDate = $attributionDate;
        $this->returnDate = $returnDate;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }
}
