<?php

namespace App\DTOs\Participants;

use Illuminate\Support\Collection;

/*
 * Class NestedParticipantDTO
 * @package App\DTOs\Participants
 *
 * Data Transfer Object for Nested Participant, to be used in other DTOs
 */
class NestedParticipantDTO {
    public int $id;
    public Collection $tickets;
    public ?string $firstName;
    public ?string $lastName;
    public ?string $email;
    public ?string $phone;
    public ?string $streetName;
    public ?string $houseNumber;
    public ?string $zipCode;
    public ?string $city;
    public ?string $state;
    public ?string $country;
    public ?string $dateOfBirth;
    public ?string $information;


    public function __construct(
        int $id,
        Collection $tickets,
        ?string $firstName = null,
        ?string $lastName= null,
        ?string $email= null,
        ?string $phone= null,
        ?string $streetName= null,
        ?string $houseNumber= null,
        ?string $zipCode= null,
        ?string $city= null,
        ?string $state= null,
        ?string $country= null,
        ?string $dateOfBirth= null,
        ?string $information= null
    ) {
        $this->id = $id;
        $this->tickets = $tickets;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phone = $phone;
        $this->streetName = $streetName;
        $this->houseNumber = $houseNumber;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->dateOfBirth = $dateOfBirth;
        $this->information = $information;
    }
}
