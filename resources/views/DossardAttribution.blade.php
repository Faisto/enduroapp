
@extends('templateNavBar')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Attribution des dossards</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    </head>
    <body>
    @section('content')
        <div class="container mt-5">
            <h1 class="display-4" style="color: #333; font-weight: bold; text-transform: uppercase; margin-bottom: 20px;">Attribution des Dossards</h1>
            <p class="lead" style="color: #666; font-size: 18px;">Identifiant de l'event : {{$test['idevent']}}</p>
            <p class="lead" style="color: #666; font-size: 18px;">Identifiant du Ticket :{{$test['idTicket']}}</p>

            <div class="card p-3 bg-body bg-gradient">
                <h2>Formulaire</h2>
                <form action="/events/{{$test['idevent']}}/tickets/{{$test['idTicket']}}/dossard/attribution" method="post">
                    @csrf

                    <!-- Background-color selection -->
                    <div class="mb-3">
                        <label class="form-label" for="bg_color">Couleur de fond</label>

                        <div id="bg_color" class="btn-group form-control" role="group">
                            <input type="radio" class="btn-check" name="bg_color" id="bg_color_vert" autocomplete="off" value="green" checked>
                            <label class="btn btn-outline-success" for="bg_color_vert">Vert</label>

                            <input type="radio" class="btn-check" name="bg_color" id="bg_color_rouge" autocomplete="off" value="red">
                            <label class="btn btn-outline-danger" for="bg_color_rouge">Rouge</label>

                            <input type="radio" class="btn-check" name="bg_color" id="bg_color_bleu" autocomplete="off" value="blue">
                            <label class="btn btn-outline-info" for="bg_color_bleu">Bleu</label>
                        </div>
                    </div>

                    <!-- Text-color selection -->
                    <div class="mb-3 ">
                        <label class="form-label" for="color_number">Couleur du texte</label>

                        <div id="color_number" class="btn-group form-control" role="group">
                            <input type="radio" class="btn-check" name="color_number" id="color_number_blanc" autocomplete="off" value="white" checked>
                            <label class="btn btn-outline-primary" for="color_number_blanc">Blanc</label>

                            <input type="radio" class="btn-check" name="color_number" id="color_number_noir" autocomplete="off" value="black">
                            <label class="btn btn-outline-dark" for="color_number_noir">Noir</label>
                        </div>
                    </div>

                    <!-- Number input -->
                    <div class="mb-3">
                        <label class="form-label" for="number">Numéro du dossard</label>
                        <input type="number" required class="form-control" id="number" name="number" placeholder="Insérez le numéro du dossard" min="1" max="200">
                    </div>

                    <button type="submit" class="btn btn-primary">Valider</button>
                </form>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    @endsection
    </body>
</html>
