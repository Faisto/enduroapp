@php use App\Enums\Dossards\DossardColorNumber @endphp

@extends('templateNavBar')

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dossards - Evenement {{ $event->id }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
@section('content')


@php(
$backgroundColorsMap = [
    'red' => 'Rouge',
    'blue' => 'Bleu',
    'green' => 'Vert',
]
)

@php(
    $colorNumbersMap = [
        'black' => 'Noir',
        'white' => 'Blanc',
    ]
)

@php(
    $colors = [
        'red' => '#FF5349',
        'blue' => '#598BAF',
        'green' => '#27b376',
        'black' => '#000000',
        'white' => '#FFFFFF',
    ]
)

<!-- Form to search for a dossard -->
<form method="GET" action="{{ route('events.dossards.index', ['event' => $event]) }}" class="p-3">
    <div class="mb-3">
        <label for="number" class="form-label">
            Numéro du dossard
        </label>
        <input
            type="number"
            id="number"
            name="number"
            placeholder="Insérez le numéro du dossard"
            value="{{ old('number') }}"
            class="form-control"
            min="1"
            max="200"
        >
    </div>

    <!-- Color radio buttons, with none option -->
    @php($backgroundColorsQuery = old('background-color') ?? '')
    <div class="mb-3">
        <label for="search-background-color">
            Couleur du dossard
        </label>

        <div id="search-background-color" class="btn-group form-control" role="group">
            <input
                type="radio"
                name="background-color"
                value=""
                id="search-background-color-none"
                class="btn-check"
                @checked($backgroundColorsQuery == '')
            >
            <label
                for="search-background-color-none"
                class="btn btn-outline-primary"
            >
                Toutes
            </label>


            @foreach(\App\Enums\Dossards\DossardBackgroundColor::cases() as $backgroundColor)
                <input
                    type="radio"
                    name="background-color"
                    value="{{ $backgroundColor->value }}"
                    id="search-background-color-{{ $backgroundColor->value }}"
                    class="btn-check"
                    @checked($backgroundColorsQuery == $backgroundColor->value)
                />
                <label
                    for="search-background-color-{{ $backgroundColor->value }}"
                    class="btn btn-outline-primary"
                >
                    {{ $backgroundColorsMap[$backgroundColor->value] }}
                </label>
            @endforeach
        </div>
    </div>

    @php($colorNumbersQuery = old('color-number') ?? '')
    <div class="mb-3">
        <label for="search-color-number">
            Couleur du numéro du dossard
        </label>

        <div id="search-color-number" class="btn-group form-control" role="group">
            <input
                type="radio"
                name="color-number"
                value=""
                id="search-color-number-none"
                class="btn-check"
                @checked($colorNumbersQuery == '')
            >
            <label
                for="search-color-number-none"
                class="btn btn-outline-primary"
            >
                Tous
            </label>

            @foreach(DossardColorNumber::cases() as $dossardColorNumber)
                <input
                    type="radio"
                    name="color-number"
                    value="{{ $dossardColorNumber->value }}"
                    id="search-color-number-{{ $dossardColorNumber->value }}"
                    class="btn-check"
                    @checked($colorNumbersQuery == $dossardColorNumber->value)
                />
                <label
                    for="search-color-number-{{ $dossardColorNumber->value }}"
                    class="btn btn-outline-primary"
                >
                    {{ $colorNumbersMap[$dossardColorNumber->value] }}
                </label>
            @endforeach
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Search</button>

</form>

<!-- Dossards -->
<section>
    @if ($dossards->isEmpty())
        <div>
            <p>No dossard found</p>
        </div>
    @else
        <div class="container">
            <div class="row row-cols-2 gx-0 gy-4">
                @foreach ($dossards as $dossard)
                    @php($backgroundColor = $backgroundColorsMap[$dossard->backgroundColor->value] ?? 'Inconnue')
                    @php($colorNumber = $colorNumbersMap[$dossard->colorNumber->value] ?? 'Inconnue')
                    @php($firstParticipant = $dossard->ticket->participants->first())

                    <div class="card">
                        <div class="card-img-top">
                            <div style="background-color: {{ $colors[$dossard->backgroundColor->value] }}"
                                 class="p-4 d-flex align-items-center justify-content-center">
                                <span class="fs-2 fw-semi-bold"
                                      style="color: {{ $dossard->colorNumber }}">{{ $dossard->number }}</span>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            @if($firstParticipant)
                                <p>
                                    <b>Participant</b>: {{ strtoupper($firstParticipant->lastName) }} {{ $firstParticipant->firstName }}
                                </p>
                            @endif

                            <p><b>Commentaire sur le ticket</b>: {{$dossard->ticket->comment}}</p>

                            <p><b>Numéro du dossard</b>: {{ $dossard->number }} </p>
                            <p><b>Couleur du dossard</b>: {{ $backgroundColor }} </p>
                            <p><b>Couleur du numéro du dossard</b>: {{ $colorNumber }}</p>

                            <form method="POST"
                                  action="{{ route('events.dossards.return', ['event' => $event, 'dossard' => $dossard->id]) }}"
                                  class="d-flex justify-content-center">
                                @csrf
                                @method('PATCH')
                                <input type="submit" value="Caution remboursée"
                                       class="btn btn-primary justify-self-center">
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</section>
@endsection
</body>
</html>
