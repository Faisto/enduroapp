@extends('templateNavBar')

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion ticket</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
@section('content')
    <div>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </div>
        @endif
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">Recherche du ticket</span>
        </div>

        <div  class="FormTicketSearch d-flex">
            <form class="d-flex container-fluid" role="search"  method="post" action="/event/ticket/control">
                @csrf
                <div>
                    <input class="form-control me-2" type="search" placeholder="Numero du ticket" aria-label="Search" name ='ticketNumber' tag="ticketNumber">
                    @error("ticketNumber")
                        {{$message.'ERROR'}}
                    @enderror
                </div>
                <button class="btn btn-outline-success" type="submit">Voir</button>
            </form>
        </div>
    </div>
@endsection
</body>
</html>

