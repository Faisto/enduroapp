<!-- Besoin de voir le Nom et Prenom de l'acheteur     -->
<!-- Besoin de voir le numero du ticket     -->
<!-- Besoin de voir le type de véhicule     -->
<!-- Besoin de voir et modifier la plaque du véhicule    -->
<!-- Besoin de voir et modifier si payé-->
<!-- Besoin de valider la remise des tickets repas     -->

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion ticket</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
<div >
    @yield('navBar')
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('homeRoute')}}">Home</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ '/event/' }}">Evenement</a>
                    </li>
                    @if(session()->has('idEvent'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ '/event/'.request()->session()->get('idEvent')}}">verification</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ '/events/'.request()->session()->get('idEvent').'/dossards'}}">recuperation</a>
                    </li>
                        <li class="nav-item">
                         <p class="navbar-text" > Evenement en cours : {{ request()->session()->get('idEvent') }}</p>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

    </nav>
    @yield('content')
    <div></div>

</Div>
</body>
</html>
