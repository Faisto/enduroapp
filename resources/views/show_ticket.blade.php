<!-- Besoin de voir le Nom et Prenom de l'acheteur     -->
<!-- Besoin de voir le numero du ticket     -->
<!-- Besoin de voir le type de véhicule     -->
<!-- Besoin de voir et modifier la plaque du véhicule    -->
<!-- Besoin de voir et modifier si payé-->
<!-- Besoin de valider la remise des tickets repas     -->
@extends('templateNavBar')
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion ticket</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
<div>
    @section('content')
    <div class="container-fluid">
        <span class="navbar-brand mb-0 h1">Information ticket</span>
    </div>
        <div  class="FormTicketSearch d-flex">
            <form class="d-flex container-fluid" method="post" action="{{ route('validateTicket', ['id' => request()->session()->get('idEvent'), 'id_ticket' => $ticket->ticket_number]) }}">
                @csrf

                <div class="row">
                    <div class="col-12">
                        <p> <b>Nom</b> : {{ $participant->last_name }}</p>
                    </div>
                    <div class="col-13">
                        <p><b>Prénom</b> : {{ $participant->first_name }}</p>
                    </div>
                    <div class="col-12">
                        <p class="" type="search"><b>Plaque du véhicule</b>: {{ $vehicle->plate_number }} </p>
                    </div>
                    <div class="col-13">
                        <p class="me-2 " type="search"><b>Type du ticket</b> : {{ $ticketType->name }} </p>
                    </div>
                    <div class="col-12">
                        <p class="me-2 " type="search"><b>Prix du ticket</b> : {{ $ticketType->price }} </p>
                    </div>
                    <div class="col-12">
                        <p class="me-2 " type="search"><b>Statut du paiement</b> : {{ $isPaid ? 'Payé' : 'Non payé' }} </p>
                    </div>

                    <hr>

                    <div class="col-12">
                        <div class="d-block mb-2" >
                            <label class="form-check-label  @if(!$isPaid)text-danger @endif" for="flexCheckPaid" >
                                En ordre de paiement :
                            </label>
                            <input class="form-check-input" type="checkbox" @if($isPaid) checked @endif id="flexCheckPaid" name="flexCheckPaid">
                        </div>
                        <div class="d-block  mb-2">
                            <label class="form-check-label" for="flexCheckRepas">
                                Ticket repas remis :
                            </label>
                            <input class="form-check-input" type="checkbox" id="flexCheckRepas" name="flexCheckRepas" >
                        </div>
                    </div>

                    <div class="col-12 mb-3">
                        <label class="form-label" for="comment">Un commentaire sur le ticket ? (Plaque incorrecte, ...)</label>
                        <textarea class="form-control me-2 " type="search"  aria-label="Search" name ='comment' placeholder="Commentaire :">{{ $ticket->comment }} </textarea>
                    </div>

                    <div class="col-12 mb-3">
                        <a class="btn btn-outline-success" href="{{ route('selectTicket', ['idEvent' => request()->session()->get('idEvent')]) }}" class="btn btn-primary">Retour</a>
                        <input class="btn btn-primary" type="submit" value="valider"/>
                    </div>

                    <hr>

                    <div class="col-12">
                        <p><b>Rappel</b>:</p>
                        <p>
                            Participant:
                            <ul>
                                <li>1 jeton Repas</li>
                                <li>1 jeton déjeuner</li>
                                <li>1 jeton boisson</li>
                            </ul>

                            Passager:
                            <ul>
                                <li>1 jeton Repas</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </form>
        </div>
</div>
@endsection
</body>
</html>
