
@extends('templateNavBar')
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion Event</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>

<body>
@Section('content')
        <div  class="FormTicketSearch d-flex">
            <div class="container-fluid">
                <span class="navbar-brand mb-0 h1">Recherche de l'event</span>
            </div>
            <form class="d-flex container-fluid" role="search"  method="post" action="/event/controlEvent">
                @csrf
                <input class="form-control me-2" type="search" placeholder="EventID" aria-label="Search" name ='idEvent' value="{{$firstEvent->id}}">
                <a class="btn btn-dark" href="{{ route('homeRoute') }}" class="btn btn-primary">Retour</a>
                <button class="btn btn-outline-success" type="submit">Valider</button>
            </form>
        </div>
@endsection
</body>
</html>
