
<html>
    <head>
        <title>Validation</title>
    </head>
    <body>
        <h1>Validation</h1>
        <label>Hi {{ $participant->first_name }},</label>
        <p>your ticket is actually: {{ $ticket->payment_status }}</p>
        <form action="{{ route('validateTicket', ['id' => 1, 'id_ticket' => $ticket->ticket_number]) }}" method="post">
            @csrf
            <input type="submit" value="validate">
        </form>
    </body>
</html>
