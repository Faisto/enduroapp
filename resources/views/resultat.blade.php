<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recherche de Dossard</title>
    <!-- fichiers CSS de Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-5">
        <div class="card">
            <div class="card-header">
                <h1 class="text-center">Résultats de la recherche</h1>
            </div>
            <div class="card-body">
                <p><strong>Numéro de dossard:</strong> {{ $dossard->number }}</p>
                <p><strong>Couleur du numéro:</strong> {{ $dossard->color_number }}</p>
                <p><strong>Couleur de fond:</strong> {{ $dossard->background_color }}</p>
                <p><strong>Date d'attribution:</strong> {{ $dossard->attribution_date }}</p>
                <p><strong>Date de retour:</strong> {{ $dossard->return_date }}</p>
                <p><strong>Numéro de ticket:</strong> {{ $dossard->id_ticket }}</p>

                <form action="/refund" method="POST">
                    @csrf
                    <input type="hidden" name="dossard_id" value="{{ $dossard->id }}">
                    <button type="submit" class="btn btn-primary">Remboursé</button>
                </form>
                <a href="/events" class="btn btn-secondary mt-3">Retour aux Events</a>
            </div>
        </div>
    </div>
    <!--  fichiers JS de Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
