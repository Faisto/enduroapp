<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Views\DossardViewController;

// Event routes

Route::prefix('events')
    ->group(function () {
        Route::get('/{event}/dossards', [DossardViewController::class, 'index'])->name('events.dossards.index');
        Route::patch('/{event}/dossards/{dossard}/return', [DossardViewController::class, 'return'])->name('events.dossards.return');
    }
);
