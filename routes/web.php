<?php

use App\Http\Controllers\ticketController;
use App\Http\Controllers\dossardsController;
use Illuminate\Support\Facades\Route;
use App\Models\ticket_types;

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/events',[\App\Http\Controllers\eventsController::class,'indexEvents']);

Route::post('/events/{idEvent}/tickets/{idTickets}/dossard/attribution',[\App\Http\Controllers\dossardsController::class,'linkDossardToTicket']);

Route::get('/events/{idEvent}/tickets/{idTickets}/dossard',[\App\Http\Controllers\dossardsController::class,'DossardAttributionView']);

Route::get('/events/{idEvent}',[\App\Http\Controllers\eventsController::class,'Events']);


Route::get('/events/{id}/tickets/{id_ticket}',function ($id, $id_ticket){
    $ticketController = new ticketController();
    $ticket = $ticketController->getTicketById($id_ticket);
    if ($ticket == null) {
        return redirect('/event/'.$id)->withErrors('Ticket not found');
    }
    $ticketType = $ticket->ticket_types()->get();
    $participant = $ticket->participants()->get();
    $vehicle = $ticket->vehicles()->get();
    $isPaid = false;
    if ($ticket->getAttribute('payment_status') == 'paid') {
        $isPaid = true;
    }
    return view('show_ticket', ['ticket' => $ticket, 'participant' => $participant[0], 'vehicle' => $vehicle[0], 'isPaid' => $isPaid, 'ticketType' => $ticketType[0]]);
})->name('showTicket');

Route::get('/events/{id}/ticketsByParticipants/{participant_last_name}/{participant_first_name}',function ($id, $participant_last_name, $participant_first_name){
    $ticketController = new ticketController();
    $ticket = $ticketController->findByParticipantName($participant_last_name, $participant_first_name);
    $ticket->save();
    return view('ticketValidation', ['ticket' => $ticket]);
});

Route::post('/events/{id}/tickets/{id_ticket}/validate',function ($id, $id_ticket){
    $ticketController = new ticketController();
    $ticketController->validateTicket($id_ticket, \App\Enums\payementStatus::paid, $_POST['comment']);
    return redirect('/events/'.$id.'/tickets/'.$id_ticket.'/dossard');
})->name('validateTicket');

Route::get('/sortie', function () { return view('sortie');});
Route::post('/search', [dossardsController::class, 'search']);
Route::post('/refund', [dossardsController::class, 'refund']);

Route::get('/event', [App\Http\Controllers\GestionEventTicketsController::class,'showSelectEventViews' ])->name('selectEventRoute');

Route::post('/event/controlEvent', [App\Http\Controllers\GestionEventTicketsController::class,'selectEvent' ]);

//Gestion select Ticket
Route::get('/event/{idEvent}', function (){
    return view('select_ticket');
})->name('selectTicket');

route::post('/event/ticket/control',  [App\Http\Controllers\GestionEventTicketsController::class,'selectTicketControl']);


route::get('/event/{idEvents}/tickets/{idTicket}', function($idEvent, $idTicket){
    return view('show_ticket');
} );

route::get('/home', function (){

    return view('templateNavBar');
})->name('homeRoute');

//route::get('/event/ticket/controlPost', [App\Http\Controllers\GestionEventTicketsController::class,'selectTicketControlPost']);
/*route::get('/event/{idEvent}/tickets/Reject', function($idEvent, $idTicket){
   return view('rejected_ticket');
}); */

// Add the routes for events
require __DIR__ . '/web/events.php';
