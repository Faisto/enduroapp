<?php

namespace Database\Factories;

use App\Models\workers;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<workers>
 */
class workersFactory extends Factory
{
    protected $model = workers::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'mail' => $this->faker->email(),
            'phone_number' => $this->faker->phoneNumber,
            'information' => $this->faker->text,
        ];
    }
}
