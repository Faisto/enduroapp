<?php

namespace Database\Factories;

use App\Enums\vehicleTypes;
use App\Models\vehicles;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<vehicles>
 */
class vehiclesFactory extends Factory
{
    protected $model = vehicles::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'type' => $this->faker->randomElement(vehicleTypes::cases())->value,
            'plate_number' => $this->faker->word,
        ];
    }
}
