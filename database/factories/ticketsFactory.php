<?php

namespace Database\Factories;

use App\Enums\payementStatus;
use App\Models\Model;
use App\Models\tickets;
use Illuminate\Database\Eloquent\Factories\Factory;
use PHPUnit\Framework\Attributes\Ticket;

/**
 * @extends Factory<Ticket>
 */
class ticketsFactory extends Factory
{
    protected $model = tickets::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'ticket_number' => $this->faker->unique()->randomNumber(8),
            'ticket_types_id' => function () {
                return \App\Models\ticket_types::factory()->create()->id;
            },
            'vehicles_id' => function () {
                return \App\Models\vehicles::factory()->create()->id;
            },
            'payment_status' => $this->faker->randomElement(payementStatus::cases())->value,
            'order_number' => $this->faker->unique()->text(10),
            'order_date' => $this->faker->date(),
            'meal_tickets' => $this->faker->randomNumber(2),
            'comment' => $this->faker->text
        ];
    }
}
