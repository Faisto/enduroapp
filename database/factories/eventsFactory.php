<?php

namespace Database\Factories;

use App\Models\events;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<events>
 */
class eventsFactory extends Factory
{
    protected $model = events::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'name' => $this->faker->word,
            'start_date' => $this->faker->date(),
            'end_date' => $this->faker->date(),
            'inscription_date' => $this->faker->date(),
            'vip_date' => $this->faker->date(),
            'description' => $this->faker->text,
            'information' => $this->faker->text
        ];
    }
}
