<?php

namespace Database\Factories;

use App\Models\participants;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<participants>
 */
class participantsFactory extends Factory
{
    protected $model = participants::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'mail' => $this->faker->email(),
            'street_name' => $this->faker->streetName(),
            'house_number' => $this->faker->randomNumber(4),
            'zip_code' => $this->faker->randomNumber(5),
            'city' => $this->faker->city,
            'state' => $this->faker->word,
            'country' => $this->faker->country,
            'phone_number' => $this->faker->phoneNumber(),
            'date_of_birth' => $this->faker->date(),
            'information' => $this->faker->text
        ];
    }
}
