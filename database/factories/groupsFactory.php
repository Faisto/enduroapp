<?php

namespace Database\Factories;

use App\Models\groups;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<groups>
 */
class groupsFactory extends Factory
{
    protected $model = groups::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'name' => $this->faker->word,
            'description' => $this->faker->text
        ];
    }
}
