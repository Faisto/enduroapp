<?php

namespace Database\Factories;

use App\Models\ticket_types;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ticket_types>
 */
class ticket_typesFactory extends Factory
{
    protected $model = ticket_types::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'name' => $this->faker->word,
            'price' => $this->faker->randomFloat(2, 0, 999999.99),
            'event_id' => function () {
                return \App\Models\events::factory()->create()->id;
            }
        ];
    }
}
