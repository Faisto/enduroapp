<?php

namespace Database\Factories;

use App\Enums\dossardsColor;
use App\Enums\dossardsNumberColor;
use App\Enums\payementStatus;
use App\Models\dossards;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<dossards>
 */
class dossardsFactory extends Factory
{
    protected $model = dossards::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'id_ticket' => function () : int {
                return \App\Models\tickets::factory()->create()->id;
            },
            'number' => $this->faker->unique()->randomNumber(8),
            'color_number' => $this->faker->randomElement(dossardsNumberColor::cases())->value,
            'background_color' => $this->faker->randomElement(dossardsColor::cases())->value,
            'attribution_date' => $this->faker->date(),
            'return_date' => $this->faker->date()
        ];
    }
}
