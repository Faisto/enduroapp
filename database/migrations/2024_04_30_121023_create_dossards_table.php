<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dossards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_ticket')->nullable()->constrained('tickets');
            $table->integer('number')->nullable();
            $table->string('color_number')->nullable();
            $table->string('background_color')->nullable();
            $table->dateTime('attribution_date')->nullable();
            $table->dateTime('return_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dossards');
    }
};
