<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('participants_tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('participants_id')->references('id')->on('participants')->cascadeOnDelete();
            $table->foreignId('tickets_id')->references('id')->on('tickets')->cascadeOnDelete();
            $table->string('role_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role_participants_tickets');
    }
};
