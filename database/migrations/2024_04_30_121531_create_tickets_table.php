<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('ticket_number')->nullable();
            $table->foreignId('vehicles_id')->nullable()->constrained('vehicles');
            $table->foreignId('ticket_types_id')->nullable()->constrained('ticket_types');
            $table->string('payment_status')->nullable();
            $table->string('order_number')->nullable();
            $table->dateTime('order_date')->nullable();
            $table->integer('meal_tickets')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
